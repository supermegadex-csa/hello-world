// Define main class
public class Main {
  public static void main(String[] args) {

    // Create a messages list with lones to print
    String[] messages = {
      "Hello, world! I'm Daniel.",
      // Empty string = empty line
      "",
      "I like to program in my free time.",
      "I also act in the Marian Musicals and plays.",
      "I have three cats: TJ, Jasper, and Draco.",
      // Shameless plug
      "Check out my projects at https://github.com/Supermegadex"
    };

    // Loop through messages and print each of them
    for (String message : messages) {
      System.out.println(message);
    }
  }
}
